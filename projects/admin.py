from django.contrib import admin
from projects.models import Project

admin.site.register(Project)


class RecipeAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )
